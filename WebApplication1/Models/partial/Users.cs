﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
       [MetadataType(typeof(UsersMetadata))]
    public partial class Users
    {
    }
    public class UsersMetadata
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("標題")]
        [StringLength(50)]
        public string Title { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("內容")]
        [StringLength(50)]
       

        public string Content { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("發布日期")]
       

        public System.DateTime PublishDate { get; set; }
    }
}